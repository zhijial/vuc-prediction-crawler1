# base image
FROM python:3.6.4

RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list

MAINTAINER team11

ENV DEBIAN_FRONTEND noninteractive

VOLUME [ "/opt/workspace/crawler1" ]

RUN apt-get -o Acquire::http::proxy="http://wwwproxy.unimelb.edu.au:8000/" update
RUN apt-get -o Acquire::http::proxy="http://wwwproxy.unimelb.edu.au:8000/" install python3-pip -y
RUN pip install --upgrade --proxy http://wwwproxy.unimelb.edu.au:8000 pip
RUN pip3 install --upgrade --proxy http://wwwproxy.unimelb.edu.au:8000 tweepy
RUN pip3 install --upgrade --proxy http://wwwproxy.unimelb.edu.au:8000 shapely
RUN pip3 install --upgrade --proxy http://wwwproxy.unimelb.edu.au:8000 python-twitter
RUN pip3 install --upgrade --proxy http://wwwproxy.unimelb.edu.au:8000 vaderSentiment

ENV DEBIAN_FRONTEND teletype

# start app
CMD ["python3", "/usr/src/app/crawler1.py", "python3", "/usr/src/app/writer.py"]